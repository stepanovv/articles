# Почитайка

## Репозиторий заморожен! Новые версии [тут](https://stepanovv.ru/articles/index.html)

Это репозиторий создан для совместной работы над статьями:

 * [От дизайна до кода без совещаний. Страшная сила красоты](https://gitlab.com/stepanovv/articles/blob/master/public/страшная%20сила%20красоты/статья.md)
 * [Советы про удалёнку в карантине](https://gitlab.com/stepanovv/articles/-/blob/master/public/Советы%20про%20удалёнку/советы%20про%20удалёнку.md)
 * [Шаг в виртуальность](https://gitlab.com/stepanovv/articles/-/blob/master/public/шаг%20в%20виртуальность/шаг%20в%20виртуальность.md)

Раньше я писал на [medium](https://medium.com/@stepanovv.ru), однако там появилась неприятная практика ограничения [доступа](https://wptavern.com/freecodecamp-moves-off-of-medium-after-being-pressured-to-put-articles-behind-paywalls).

Предложения и вопросы к статьям можно подать в [issues](https://gitlab.com/stepanovv/articles/issues).
Для подачи предложения необходимо пройти простую процедуру регистрации также, как и на medium.

Ну и, конечно, можно клонировать к себе статьи, [наблюдать](https://gitlab.com/stepanovv/articles/activity) за их изменениями или прислать [свои](https://gitlab.com/stepanovv/articles/merge_requests), т.к. это git. Есть [RSS](https://gitlab.com/stepanovv/articles.atom)

Статьи, в соновном, для разработчиков, потому думаю, что это вполне комфортно.

Для статей выбран формат markdown, его поддерживают многие редакторы. Кроме того, его также корректно отображает сам облачный gitlab.

Поддержать можно поставив звёздочку в gitlab или [похлопать](https://medium.com/@stepanovv.ru/) в medium.

## Сборка проекта

 * конвертировать md в html
 * переместить README.html в /public
 * git push origin deleop
 * https://gitlab.com/stepanovv/articles/-/merge_requests

## ЗЫ

 * проверено [yaspeller](https://yandex.ru/dev/speller/)
 * [Контакты автора](https://stepanovv.ru/portfolio/portfolio.html#id-contacts)
